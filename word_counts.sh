#!/bin/bash
sed -e 's/\s/\n/g' "$1" | tr '[A-Z]' '[a-z]' | tr -d '[:punct:]' | sort | uniq -c | sort -nr > word_counts.txt
